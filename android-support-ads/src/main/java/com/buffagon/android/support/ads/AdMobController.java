package com.buffagon.android.support.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.buffagon.android.support.Checker;
import com.buffagon.android.support.utils.NetworkUtils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

/**
 * ==== Описание класса ====
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public class AdMobController implements AdsController {
  private static final int REQUEST_TIMEOUT = 30000;
  private String admobId;
  private AdView adView;
  private Context context;
  private long last;
  private Checker purchaseChecker;

  public AdMobController(Context context, ViewGroup viewGroup, ViewGroup.LayoutParams layoutParams,
                         String admobId, Checker purchaseChecker) {
    this.purchaseChecker = purchaseChecker;
    init(context, viewGroup, layoutParams, admobId);
  }

  public AdMobController(Context context, ViewGroup viewGroup, ViewGroup.LayoutParams layoutParams, String admobId) {
    init(context, viewGroup, layoutParams, admobId);
  }

  private void init(Context context, ViewGroup viewGroup, ViewGroup.LayoutParams layoutParams, String admobId) {
    this.context = context;
    this.admobId = admobId;
    createView(viewGroup, layoutParams);
    last = System.currentTimeMillis() - REQUEST_TIMEOUT;
  }

  private void createView(ViewGroup viewGroup, ViewGroup.LayoutParams layoutParams) {
    if((purchaseChecker != null && purchaseChecker.check()) || !NetworkUtils.isOnline()) return;
    adView = new AdView(context);
    adView.setAdUnitId(admobId);
    adView.setAdSize(AdSize.SMART_BANNER);
    viewGroup.addView(adView, layoutParams);
    AdRequest adRequest = new AdRequest.Builder().build();
    adView.loadAd(adRequest);
  }

  @Override
  // обновляем рекламу не чаще, чем раз в 30 секунд
  public void setVisibility(boolean show) {
    if(adView == null) return;
    adView.setVisibility((show) ? View.VISIBLE : View.GONE);
    if (show && (System.currentTimeMillis() - last > REQUEST_TIMEOUT)) {
      last = System.currentTimeMillis();
      AdRequest adRequest = new AdRequest.Builder().build();
      adView.loadAd(adRequest);
    }
  }

  @Override
  public void start() {
    if(adView != null)
      adView.resume();
  }

  @Override
  public void destroy() {
    if(adView != null)
      adView.destroy();
  }

  @Override
  public void stop() {
    if(adView != null)
      adView.pause();
  }
}
