package com.buffagon.android.support.ads;

/**
 * ==== Описание класса ====
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public interface AdsController {
  void setVisibility(boolean show);
  void start();
  void destroy();
  void stop();
}
