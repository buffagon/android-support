package com.android.buffagon.android.support.billing;

import android.content.Context;
import android.content.SharedPreferences;
import com.android.buffagon.android.support.billing.iab.IabPurchase;
import com.buffagon.android.support.App;
import com.buffagon.android.support.utils.AccountUtils;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class BillingHelper {
  private static final String PURCHASE_PREFIX = "PURCHASE-";
  private static final String PREFERENCES_NAME = "PURCHASES_PREFERENCES";
  /** Verifies the developer payload of a purchase. */
  public static boolean verifyDeveloperPayload(IabPurchase iabPurchase, Purchase purchase) {
    return getPayload(purchase).equals(iabPurchase.getDeveloperPayload());
  }

  public static String getPayload(Purchase purchase) {
    return AccountUtils.getGoogleAccountEmail() + "-" + App.getInstance().getPackageName() + "-" + purchase.getSku();
  }

  public static void savePurchase(Purchase purchase, boolean value) {
    SharedPreferences preferences = App.getInstance().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putBoolean(PURCHASE_PREFIX + purchase.getSku(), value);
    editor.commit();
  }

  public static boolean isPurchased(Purchase purchase) {
    SharedPreferences preferences = App.getInstance().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    return preferences.getBoolean(PURCHASE_PREFIX + purchase.getSku(), false);
  }
}
