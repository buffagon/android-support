package com.android.buffagon.android.support.billing;

import com.buffagon.android.support.Checker;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public class PurchaseChecker implements Checker {
  private Purchase purchase;

  public PurchaseChecker(Purchase purchase) {
    this.purchase = purchase;
  }

  @Override
  public boolean check() {
    return BillingHelper.isPurchased(purchase);
  }
}
