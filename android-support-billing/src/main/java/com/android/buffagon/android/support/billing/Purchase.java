package com.android.buffagon.android.support.billing;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public interface Purchase {
  String getSku();
  String getName();
  String getDescription();
}
