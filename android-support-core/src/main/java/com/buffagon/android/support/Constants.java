package com.buffagon.android.support;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class Constants {
  public static final String PREF_LANGUAGE = "PREF_LANGUAGE";
}
