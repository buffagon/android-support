package com.buffagon.android.support.utils;

import java.lang.reflect.Field;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class ResourcesUtils {
  public static int getResourceIdFromString(String name, Class<?> clazz) {
    try {
      Field field = clazz.getDeclaredField(name);
      return field.getInt(null);
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

}
