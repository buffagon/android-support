package com.buffagon.android.support.utils;

import java.util.Collection;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class StringUtils {
  public static String join(Collection<String> collection, String separator) {
    StringBuilder builder = new StringBuilder();
    boolean first = true;
    for(String str : collection)
      if(first) {
        builder.append(str);
        first = false;
      } else {
        builder.append(separator).append(str);
      }
    return builder.toString();
  }
}
