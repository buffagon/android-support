package com.buffagon.android.support.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import com.buffagon.android.support.R;
import com.buffagon.android.support.objs.Index;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public class Cell extends View {
  private static final int[] STATE_POSSIBLE = {R.attr.state_possible};

  private TextPaint textPaint;
  private Index position;
  private String text;
  private Drawable drawable;
  private boolean possible;
  private float cellPadding;


  public Cell(Context context, Index position) {
    super(context);
    init(context);
    this.position = position;
  }

  public Cell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  private void init(Context context) {
    Resources resources = context.getResources();
    drawable = resources.getDrawable(android.R.drawable.btn_default);
    textPaint = new TextPaint();
    textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    textPaint.setColor(Color.WHITE);
    textPaint.setTextAlign(Paint.Align.CENTER);
    textPaint.setAntiAlias(true);
    textPaint.setTypeface(Typeface.DEFAULT_BOLD);
    cellPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, resources.getDisplayMetrics());
    possible = false;
    text = "";
  }

  public Drawable getDrawable() {
    return drawable;
  }

  public void setDrawable(Drawable drawable) {
    this.drawable = drawable;
  }

  public float getCellPadding() {
    return cellPadding;
  }

  public void setCellPadding(float cellPadding) {
    this.cellPadding = cellPadding;
  }

  public Index getPosition() {
    return position;
  }

  public void setPosition(Index position) {
    this.position = position;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
    invalidate();
  }

  public boolean isPossible() {
    return possible;
  }

  public void setPossible(boolean possible) {
    this.possible = possible;
    refreshDrawableState();
  }

  @Override
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    drawable.setState(getDrawableState());
    invalidate();
  }

  @Override
  protected int[] onCreateDrawableState(int extraSpace) {
    final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
    if (possible)
      mergeDrawableStates(drawableState, STATE_POSSIBLE);
    return drawableState;
  }

  @Override
  protected void onSizeChanged(int width, int height, int oldwidth, int oldheight) {
    super.onSizeChanged(width, height, oldwidth, oldheight);
    drawable.setBounds(0, 0, width, height);
    textPaint.setTextSize(width - cellPadding * 2);
  }

  @Override
  protected void onDraw(Canvas canvas) {
    drawable.draw(canvas);
    if(!text.equals("")) {
      int xPos = (getWidth() / 2);
      int yPos = (int) ((getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2)) + 1 ;
      canvas.drawText(text, xPos, yPos, textPaint);
    }
  }


//  @Override
//  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//    int min = Math.min(widthMeasureSpec, heightMeasureSpec);
//    super.onMeasure(min, min);
//  }
}
