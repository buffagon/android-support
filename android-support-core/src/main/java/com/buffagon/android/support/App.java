package com.buffagon.android.support;

import android.app.Application;

/**
 * Расширенный класс приложения для статического доступа к контексту приложения.
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public class App extends Application {
  private static Application instance;

  public static Application getInstance() {
    return instance;
  }

  @Override
  public void onCreate() {
    instance = this;
  }
}
