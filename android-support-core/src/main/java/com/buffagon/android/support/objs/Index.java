package com.buffagon.android.support.objs;

import java.io.Serializable;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public class Index implements Serializable {
  public int i;
  public int j;

  public Index() {
  }

  public Index(int i, int j) {
    this.i = i;
    this.j = j;
  }
}
