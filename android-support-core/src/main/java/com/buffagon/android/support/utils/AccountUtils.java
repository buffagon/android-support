package com.buffagon.android.support.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import com.buffagon.android.support.App;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class AccountUtils {
  public static String getGoogleAccountEmail() {
    AccountManager manager = (AccountManager) App.getInstance().getSystemService(Context.ACCOUNT_SERVICE);
    for(Account account: manager.getAccounts())
      if(account.type.equalsIgnoreCase("com.google"))
        return account.name;
    return "";
  }
}
