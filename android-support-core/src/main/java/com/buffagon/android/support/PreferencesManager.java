package com.buffagon.android.support;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public final class PreferencesManager {

  public static void initDefaultPreferences(Map<String, ?> defaultValues) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    Set<String> exists;
    Map<String, ?> map = preferences.getAll();
    if(map == null || map.isEmpty())
      exists = new HashSet<String>(0);
    else
      exists = map.keySet();
    SharedPreferences.Editor editor = preferences.edit();
    for(Map.Entry<String, ?> entry : defaultValues.entrySet()) {
      if(exists.contains(entry.getKey()))
        continue;
      if(entry.getValue() instanceof Boolean)
        editor.putBoolean(entry.getKey(), (Boolean) entry.getValue());
      else if(entry.getValue() instanceof Integer)
        editor.putInt(entry.getKey(), (Integer) entry.getValue());
      else if(entry.getValue() instanceof String)
        editor.putString(entry.getKey(), (String) entry.getValue());
      else if(entry.getValue() instanceof Long)
        editor.putLong(entry.getKey(), (Long) entry.getValue());
      else if(entry.getValue() instanceof Float)
        editor.putFloat(entry.getKey(), (Float) entry.getValue());
    }
    editor.commit();
  }

  public static String getString(String key) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    return preferences.getString(key, "");
  }

  public static Long getLong(String key) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    return preferences.getLong(key, 0);
  }

  public static Boolean getBoolean(String key) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    return preferences.getBoolean(key, false);
  }

  public static Float getFloat(String key) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    return preferences.getFloat(key, 0);
  }

  public static Integer getInt(String key) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    return preferences.getInt(key, 0);
  }

  public static void putString(String key, String value) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(key, value);
    editor.commit();
  }

  public static void putLong(String key, Long value) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    SharedPreferences.Editor editor = preferences.edit();
    editor.putLong(key, value);
    editor.commit();
  }

  public static void putBoolean(String key, Boolean value) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    SharedPreferences.Editor editor = preferences.edit();
    editor.putBoolean(key, value);
    editor.commit();
  }

  public static void putFloat(String key, Float value) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    SharedPreferences.Editor editor = preferences.edit();
    editor.putFloat(key, value);
    editor.commit();
  }

  public static void putInt(String key, Integer value) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    SharedPreferences.Editor editor = preferences.edit();
    editor.putInt(key, value);
    editor.commit();
  }
}
