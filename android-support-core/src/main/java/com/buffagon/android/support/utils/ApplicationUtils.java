package com.buffagon.android.support.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.buffagon.android.support.App;

/**
 * ==== Описание класса ====
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class ApplicationUtils {
  public static void rateApp(Context context) {
    Intent intent = new Intent(Intent.ACTION_VIEW);
    intent.setData(Uri.parse("market://details?id=" + App.getInstance().getPackageName()));
    context.startActivity(intent);
  }
}
