package com.buffagon.android.support.utils;

import android.view.View;
import android.view.ViewGroup;

/**
 * ==== Описание класса ====
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class LayoutUtils {
  public static void setMargins (View v, int l, int t, int r, int b) {
    if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
      ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
      p.setMargins(l, t, r, b);
      v.requestLayout();
    }
  }
}
