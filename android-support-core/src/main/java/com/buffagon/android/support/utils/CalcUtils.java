package com.buffagon.android.support.utils;

import com.buffagon.android.support.objs.Index;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class CalcUtils {
  /**
   * Проверяет являются ли точки смежными.
   * @return true если точки смежные, иначе false
   */
  public static boolean isAdjacent(Index p1, Index p2) {
    return ((Math.abs(p1.i - p2.i) < 2) && (Math.abs(p1.j - p2.j) < 2));
  }
}
