package com.buffagon.android.support;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public interface Checker {
  boolean check();
}
