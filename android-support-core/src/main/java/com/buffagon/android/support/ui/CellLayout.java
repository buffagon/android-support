package com.buffagon.android.support.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.buffagon.android.support.R;


public class CellLayout extends ViewGroup {
  private int columns = 4;
  private int rows = 4;
  private int spacing = 0;
  private float cellSize;
  private float heightOffset;
  private float widthOffset;

  public CellLayout(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    initAttrs(context, attrs);
  }

  public CellLayout(Context context, AttributeSet attrs) {
    super(context, attrs);

    initAttrs(context, attrs);
  }

  public CellLayout(Context context) {
    super(context);
  }

  public void initAttrs(Context context, AttributeSet attrs) {
    TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CellLayout, 0, 0);
    try {
      columns = a.getInt(R.styleable.CellLayout_columns, 4);
      columns = a.getInt(R.styleable.CellLayout_rows, 4);
      spacing = a.getDimensionPixelSize(R.styleable.CellLayout_spacing, 0);
    } finally {
      a.recycle();
    }
  }

  public void setColumns(int columns) {
    this.columns = columns;
  }

  public void setRows(int rows) {
    this.rows = rows;
  }

  public void setSpacing(int spacing) {
    this.spacing = spacing;
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    final int width = MeasureSpec.getSize(widthMeasureSpec);
    final int height = MeasureSpec.getSize(heightMeasureSpec);

    float cellWidth = (float) (width - getPaddingLeft() - getPaddingRight()) / (float) columns;
    float cellHeight = (float) (height - getPaddingTop() - getPaddingBottom()) / (float) rows;

    if(cellHeight > cellWidth) {
      heightOffset = (cellHeight - cellWidth) * rows / 2;
      widthOffset = 0;
      cellSize = cellWidth;
    } else {
      heightOffset = 0;
      widthOffset = (cellWidth - cellHeight) * columns / 2;
      cellSize = cellHeight;
    }

    for (int i = 0; i < getChildCount(); i++) {
      View child = getChildAt(i);
      LayoutParams layoutParams = (LayoutParams) child.getLayoutParams();
      int w = layoutParams.width;
      int h = layoutParams.height;
      int childWidthSpec = MeasureSpec.makeMeasureSpec((int) (w * cellSize  - spacing * 2), MeasureSpec.EXACTLY);
      int childHeightSpec = MeasureSpec.makeMeasureSpec((int) (h * cellSize - spacing * 2), MeasureSpec.EXACTLY);
      child.measure(childWidthSpec, childHeightSpec);
    }

    setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
  }

  @Override
  protected void onLayout(boolean changed, int l, int t, int r, int b) {
    int childCount = getChildCount();

    View child;
    for (int i = 0; i < childCount; i++) {
      child = getChildAt(i);

      LayoutParams layoutParams = (LayoutParams) child.getLayoutParams();

      int top = (int) (layoutParams.top * cellSize + heightOffset) + getPaddingTop();
      int left = (int) (layoutParams.left * cellSize + widthOffset) + getPaddingLeft();
      int right = (int) (left + layoutParams.width * cellSize);
      int bottom = (int) (top + layoutParams.height * cellSize);

      child.layout(left + spacing, top + spacing, right - spacing, bottom - spacing);
    }
  }

  @Override
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
    return new CellLayout.LayoutParams(getContext(), attrs);
  }

  @Override
  protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
    return p instanceof CellLayout.LayoutParams;
  }

  @Override
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
    return new CellLayout.LayoutParams(p);
  }

  @Override
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams();
  }

  public static class LayoutParams extends ViewGroup.LayoutParams {
    int top = 0;
    int left = 0;
    int width = 1;
    int height = 1;

    public LayoutParams(Context context, AttributeSet attrs) {
      super(context, attrs);
      TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CellLayout);
      left = a.getInt(R.styleable.CellLayout_layout_left, 0);
      top = a.getInt(R.styleable.CellLayout_layout_top, 0);
      height = a.getInt(R.styleable.CellLayout_layout_cellsHeight, -1);
      width = a.getInt(R.styleable.CellLayout_layout_cellsWidth, -1);
      a.recycle();
    }

    public LayoutParams(ViewGroup.LayoutParams params) {
      super(params);
      if (params instanceof LayoutParams) {
        LayoutParams cellLayoutParams = (LayoutParams) params;
        left = cellLayoutParams.left;
        top = cellLayoutParams.top;
        height = cellLayoutParams.height;
        width = cellLayoutParams.width;
      }
    }

    public LayoutParams() {
      this(MATCH_PARENT, MATCH_PARENT);
    }

    public LayoutParams(int width, int height) {
      super(width, height);
    }

    public LayoutParams(int width, int height, int left, int top) {
      super(width, height);
      this.width = width;
      this.height = height;
      this.left = left;
      this.top = top;
    }
  }
}