package com.buffagon.android.support.db.entities;

import java.io.Serializable;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class Entity implements Serializable {
  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
