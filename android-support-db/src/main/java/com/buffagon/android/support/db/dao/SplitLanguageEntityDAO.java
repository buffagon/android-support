package com.buffagon.android.support.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.buffagon.android.support.Constants;
import com.buffagon.android.support.PreferencesManager;
import com.buffagon.android.support.db.entities.Entity;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class SplitLanguageEntityDAO<T extends Entity> extends EntityDAO<T> {
  public static final String COLUMN_LOCALE = "_locale";
  private final String locale;
  protected SplitLanguageEntityDAO(String language, String tableName, Class<T> entityClass, SQLiteDatabase database) {
    super(tableName, entityClass, database);
    this.locale = language;
  }

  protected SplitLanguageEntityDAO(String tableName, Class<T> entityClass, SQLiteDatabase database) {
    super(tableName, entityClass, database);
    this.locale = PreferencesManager.getString(Constants.PREF_LANGUAGE);
  }


  @Override
  protected Long save(T entity, ContentValues values) {
    values.put(COLUMN_LOCALE, locale);
    return super.save(entity, values);
  }

  @Override
  public boolean delete(Long id) {
    final String filter = COLUMN_ID + " = " + id + " AND " + COLUMN_LOCALE + " = '" + locale + "'";
    return database.delete(tableName, filter, null) > 0;
  }

  @Override
  public T findOne(Long id) {
    final String selectQuery = "SELECT  * FROM " + tableName + " WHERE " +
      COLUMN_ID + " = " + id + " AND " + COLUMN_LOCALE + " = '" + locale + "'";
    Cursor cursor = database.rawQuery(selectQuery, null);
    if(cursor == null || !cursor.moveToFirst())
      return null;
    T entity = cursorToEntity(cursor);
    cursor.close();
    return entity;
  }

  @Override
  public long count() {
    Cursor cursor= database.rawQuery("SELECT count(*) FROM " + tableName + " WHERE " +
                                       COLUMN_LOCALE + " = '" + locale + "'", null);
    if(cursor == null || !cursor.moveToFirst())
      return 0l;
    Long count = cursor.getLong(0);
    cursor.close();
    return count;
  }

  @Override
  public Cursor getCursorToAll() {
    return database.rawQuery("SELECT * FROM " + tableName + " WHERE " + COLUMN_LOCALE + " = '" + locale + "'",
                             null);
  }

  protected String getLocale() {
    return locale;
  }
}
