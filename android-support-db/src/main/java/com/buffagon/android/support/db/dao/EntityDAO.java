package com.buffagon.android.support.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.buffagon.android.support.db.entities.Entity;

/**
 * === Описание класса ===
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public abstract class EntityDAO<T extends Entity> {
  public static final String COLUMN_ID = "_id";

  private static final String TAG = "EntityDAO";

  protected final String tableName;
  protected SQLiteDatabase database;

  private final Class<T> entityClass;

  protected EntityDAO(String tableName, Class<T> entityClass, SQLiteDatabase database) {
    this.entityClass = entityClass;
    this.tableName = tableName;
    this.database = database;
  }

  public abstract Long save(T entity);

  protected Long save(T entity, ContentValues values) {
    if(entity.getId() == null)
      return  database.insert(tableName, null, values);
    database.update(tableName, values, COLUMN_ID + " = " + entity.getId(), null);
    return entity.getId();
  }

  public boolean delete(Long id) {
    return database.delete(tableName, COLUMN_ID + " = " + id, null) > 0;
  }

  public T findOne(Long id) {
    final String selectQuery = "SELECT  * FROM " + tableName + " WHERE " + COLUMN_ID + " = " + id;
    Cursor cursor = database.rawQuery(selectQuery, null);
    if(cursor == null || !cursor.moveToFirst())
      return null;
    T entity = cursorToEntity(cursor);
    cursor.close();
    return entity;
  }

  public long count() {
    Cursor cursor= database.rawQuery("SELECT count(*) FROM " + tableName, null);
    if(cursor == null || !cursor.moveToFirst())
      return 0l;
    Long count = cursor.getLong(0);
    cursor.close();
    return count;
  }

  public Cursor getCursorToAll() {
    return database.rawQuery("SELECT * FROM " + tableName, null);
  }

  public T cursorToEntity(Cursor cursor) {
    T entity;
    try {
      entity = entityClass.newInstance();
    } catch (Exception e) {
      Log.e(TAG, e.getMessage(), e);
      return null;
    }
    entity.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
    return entity;
  }
}
